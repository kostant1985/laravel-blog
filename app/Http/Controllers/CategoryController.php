<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Models\Category;
use App\Models\Post;

use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Category $categoryModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'slug' => 'required|unique:posts|max:255',

        ]);

        //dd($validator->fails());

        if ($validator->fails()) {
            return redirect('admin/category/create')
                ->withErrors($validator)
                ->withInput();
        }else{
            $categoryModel->create($request->all());
            return redirect()->route('posts');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $idCategory = Category::where('slug', $id)->pluck('id');
        $categoryPosts = Post::where ('categories_id','=', $idCategory)->get();
        $categories = Category::all();
        //dd($categoryPosts);

        return view('blog.show',['categoryPosts' => $categoryPosts, 'categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\Category;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        //$posts = Post::all();
        //$posts = Post::latest('id')->get();
        //$posts = Post::latest('published_at')->get();
        $posts = Post::latest('published_at')
            //->where('published_at' , '<=' , Carbon::now())
            ->get();

        $categories = Category::all();

        return view('blog.index',['posts' => $posts, 'categories' => $categories]);
    }

/*    public function unpublished (Post $postModel)
    {
        $posts = $postModel->getUnPublishedPosts();
        return view('post.index',['posts' =>  $posts]);
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $category = Category::lists('name', 'id');
        return view('admin.post.create',['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Post $postModel, Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'slug' => 'required|unique:posts|max:255',

        ]);

        //dd($validator->fails());

        if ($validator->fails()) {
            return redirect('admin/post/create')
                ->withErrors($validator)
                ->withInput();
        }else{
            $postModel->create($request->all());
            return redirect()->route('posts');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $post = Post::where('slug' , $id)->first();
        $categories = Category::all();

        return view('blog.showPost',['post' => $post, 'categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

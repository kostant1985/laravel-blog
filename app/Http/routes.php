<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', ['as' => 'posts', 'uses' => 'PostController@index']);

//get('unpublished',['as' => 'posts.unpublished', 'uses' => 'PostController@unpublished']);



//$router->resource('post', 'PostController');


get('post/{post}',      ['as' => 'post.show',   'uses' => 'PostController@show']);

get('category/{slug}',  ['as' => 'category.show',   'uses' => 'CategoryController@show']);


//


// Authentication routes...
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

# Admin area
//Route::post('admin/useradmin/store', 'Admin\AdminController@storeAdminUser');
Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin']], function() {

    get('/',  ['as' => 'admin.panel', 'uses' => 'AdminController@index']);

    get('category/create',  ['as' => 'category.create', 'uses' => 'CategoryController@create']);
    post('category/store',  ['as' => 'category.store',  'uses' => 'CategoryController@store']);

    get('post/create',      ['as' => 'post.create', 'uses' => 'PostController@create']);
    post('post/store',      ['as' => 'post.store',  'uses' => 'PostController@store']);
    get('post/{post}/edit', ['as' => 'post.edit',   'uses' => 'PostController@edit']);
    post('post/{post}',     ['as' => 'post.update', 'uses' => 'PostController@update']);


});
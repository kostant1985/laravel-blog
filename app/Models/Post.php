<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'excerpt','content', 'published_at', 'categories_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function category()
    {
        return $this->belongsTo('Category ');
    }

/*    public  function  getPublishedPosts()
    {
        $posts = $this->latest('published_at')->piblished()->get();
        return $posts;
    }

    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now())
            ->where('published_at', '=', 1);
    }

    public function scopeUnPublished($query)
    {
        $query->where('published_at', '=>', Carbon::now())
            ->orwhere('published_at', '=', 0);
    }

    public  function  getUnPublishedPosts()
    {
        $posts = $this->latest('published_at')->unPublished()->get();
        return $posts;
    }*/


}

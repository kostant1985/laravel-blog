<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use App\Models\Category;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::unguard();

        $this->call('UsersSeeder');

        Category::unguard();

        $this->call('CategorySeeder');

        Model::unguard();

        $this->call('PostsSeeder');


    }
}

class UsersSeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name' => 'admin' ,
            'password' => '$2y$10$SMtuzQ/8KGtMXzUvBjNc7.B4uQVCP7/uGeck4mhB6D3XeKG6Ten7a',
            'superuser' => true,
            'email' => 'admin@example.com',
        ]);

    }
}

class CategorySeeder extends Seeder{

    public function run()
    {
        DB::table('categories')->delete();

        for ($i = 1; $i <= 10; $i++) {
            Category::create([
                'slug' => $i . '-category' ,
                'name' => $i . 'Категория',
            ]);
        }
    }
}

class PostsSeeder extends Seeder{

    public function run()
    {
        DB::table('posts')->delete();

        for ($i = 1; $i <= 10; $i++) {
            for ($n = 1; $n <= 3; $n++) {
                Post::create([
                    'title' => $i . ' Post ',
                    'slug' => $i . '-post-' .$n,
                    'excerpt' => $i . 'Post body ',
                    'content' => $i . 'Content  Post body',
                    'published_at' => DB::raw('CURRENT_TIMESTAMP'),
                    'categories_id' => $i,
                ]);
            }
        }
    }
}


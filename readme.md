###Развертывание приложения

1. Клонировать проект git clone https://kostant1985@bitbucket.org/kostant1985/laravel-blog.git
2. Установить зависимости composer install
3. Скопировать .env.example в .env и выставить в нем свои настройки
4. php artisan key:generate
5. Поднять миграции: php artisan migrate
6. Наполнить таблицы php artisan db:seed
7. Запуск сервера php artisan serve --port=8080


зайти в админскую часть : 
/admin

email:  admin@example.com

пароль: 111111
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    <label class="col-md-4 control-label">Логин:</label>
    <div class="col-md-6">
        <input type="name" class="form-control" name="name" value="{{ old('name') }}">
    </div>
</div>

<div class="form-group">
    <label class="col-md-4 control-label">Пароль</label>
    <div class="col-md-6">
        <input type="password" class="form-control" name="password">
    </div>
</div>

{{--<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember"> Запомнить меня
            </label>
        </div>
    </div>
</div>--}}

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">

        {!! Form::submit('Войти', ['class'=>'btn btn-primary'] ) !!}

        {{--<a href="/password/email">Забыли ваш пароль?</a>--}}
    </div>
</div>
@extends('app')

@section('nav')
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ asset('/') }}">Блог</a>
        <a class="navbar-brand" href="{{ URL::route('admin.panel') }}">Админская панель</a>
        <a class="navbar-brand" href="{{ URL::route('post.create') }}">Добавить статью</a>
        {{--{!! link_to_route ('category.create', 'Добавить статью') !!}--}}
        <a class="navbar-brand" href="{{ URL::route('category.create') }}">Добавить категорию</a>
        @unless(Auth::guest()))
            <a class="navbar-brand" href="{{ asset('/auth/logout') }}">Выйти из системы</a>
        @endif
    </div>
@stop



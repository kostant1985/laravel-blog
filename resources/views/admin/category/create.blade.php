@extends('admin.adminApp')

@section('title', 'Добавление категории')

@section('content')
    <h1>Создание категорий статей</h1>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['route' => 'category.store']) !!}
        @include('admin.category._form')
    {!! Form::close() !!}
@endsection

@extends('app')

@section('nav')
    <div class="navbar-header">
        <div class="navbar-brand" >Вход для админа</div>
    </div>
@stop

@section('content')

	<div class="row row-offcanvas row-offcanvas-right">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Авторизация админа</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

                    {!! Form::open(['route' => 'admin.login','class'=>'form-horizontal']) !!}
                        @include('admin._form')
                    {!! Form::close() !!}

				</div>
			</div>
		</div>
	</div>

@endsection

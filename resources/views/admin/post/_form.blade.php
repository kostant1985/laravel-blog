<div class="form-group">
    {!! Form::label('slug') !!}
    {!! Form::text('slug', null, ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('название') !!}
    {!! Form::text(' title', null, ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('превью') !!}
    {!! Form::textarea(' excerpt', null, ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('содержание') !!}
    {!! Form::textarea('content', null, ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('опубликовано') !!}
    {!! Form::input('date', 'published_at', date('Y-m-d'), ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('категория') !!}
    {!! Form::select('categories_id', $category , ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">

    {!! Form::submit('Создать', ['class'=>'btn btn-primary'] ) !!}
</div>
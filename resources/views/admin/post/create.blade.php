@extends('admin.adminApp')

@section('title', 'Добавление статьи')


@section('content')
    <h1>Создание статьи</h1>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(['route' => 'post.store']) !!}
        @include('admin.post._form')
    {!! Form::close() !!}

@endsection

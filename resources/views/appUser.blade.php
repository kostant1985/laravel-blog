
@extends('app')

@section('sidebar')

    <div id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
        <div class="list-group">
            <div class="list-group-item active">Категории</div>
            @foreach($categories as $category)
                <a class="list-group-item" href="{{  url('category', $category->slug ) }}"> {!! $category->name !!}</a>
            @endforeach
        </div>
    </div>

@endsection

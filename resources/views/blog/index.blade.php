@extends('appUser')

@section('title', 'Блог')

@section('css')

@stop

@section('nav')
    <div class="navbar-header">
            <a class="navbar-brand" href="{{ asset('/') }}">Блог</a>
            {{--@unless(Auth::guest()))
            <a class="navbar-brand" href="{{ asset('/auth/logout') }}">Выйти из системы</a>
            @endif--}}
    </div>
@stop

@section('content')
   {{-- <div>
       {!! link_to_route ('posts', 'published') !!} &nbsp;&nbsp;&nbsp; {!! link_to_route ('posts.unpublished', 'unpublished') !!} &nbsp;&nbsp;&nbsp; {!! link_to_route ('post.create', 'new') !!}
    </div>--}}

       <div class="col-xs-12 col-sm-9">
               <div class="row">
                   @foreach($posts as $post)
                       <div class="col-xs-6 col-lg-4">
                           <h2>{!!$post->title!!}</h2>

                           <p>published: {{$post->published_at}}</p>

                           <p>{!!$post->excerpt!!}</p>
                           <p>
                               <a class="btn btn-default" role="button" href="{{  url('post', $post->slug ) }}">Подробнее »</a>
                           </p>
                       </div>
                   @endforeach
               </div>
       </div>



@stop

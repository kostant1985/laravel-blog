@extends('appUser')

@section('title', 'Блог')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('/base/blog.css') }}" />
@stop

@section('nav')
    <div class="navbar-header">
            <a class="navbar-brand" href="{{ asset('/') }}">Блог</a>
    </div>
@stop

@section('content')

       <div class="col-xs-12 col-sm-9">
               <div class="row">
                   @foreach($categoryPosts as $post)
                       <div class="col-xs-6 col-lg-4">
                           <h2>{!!$post->title!!}</h2>

                           <p>published: {{$post->published_at}}</p>

                           <p>{!!$post->excerpt!!}</p>
                           <p>
                               <a class="btn btn-default" role="button" href="{{  url('post', $post->slug ) }}">Подробнее »</a>
                           </p>
                       </div>
                   @endforeach
               </div>
       </div>

@stop

@extends('app')

@section('title', 'Блог')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('/base/blog.css') }}" />
@stop

@section('nav')
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ asset('/') }}">Блог</a>
    </div>
@stop

@section('content')
   {{-- <div>
       {!! link_to_route ('posts', 'published') !!} &nbsp;&nbsp;&nbsp; {!! link_to_route ('posts.unpublished', 'unpublished') !!} &nbsp;&nbsp;&nbsp; {!! link_to_route ('post.create', 'new') !!}
    </div>--}}
   <div class="row row-offcanvas row-offcanvas-right">
       <div class="col-xs-12 col-sm-9">
               <div class="row">
                   <h2>{!!$post->title!!}</h2>

                   <p>published: {{$post->published_at}}</p>

                   <p>{!!$post->excerpt!!}</p>

               </div>
       </div>
   </div>
@stop
